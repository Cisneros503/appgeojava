package accesodatos;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConexionSql {
    Connection conexion = null;
    static ConexionSql instancia = null;
    //Aca muchachos pueden solo cambiar a su server y al puert 1433 en mi maquina lo configure diferente
    //String url = "jdbc:sqlserver://DESKTOP-17O91CP\\SQLEXPRESS:1433;databaseName=appGeoLocation"; //CNN LOCAL
    String url = "jdbc:sqlserver://appGeoLocation.mssql.somee.com;databaseName=appGeoLocation";//CNN EXTERNA
    
    //Constructor de la clase
    public ConexionSql() throws Exception{
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        //conexion = DriverManager.getConnection(url, "sa", "12345");//CNN LOCAL
        conexion = DriverManager.getConnection(url, "laurence_SQLLogin_1", "Laurence2013@");//CNN EXTERNA
    }
    //Creando una instancia de la clase
    public static ConexionSql getInstancia() throws Exception{
        if (instancia== null)
            instancia = new ConexionSql();//Invocación del constructor
        return instancia;
    }
    public Connection getConexion(){
        return conexion;
    }
}
